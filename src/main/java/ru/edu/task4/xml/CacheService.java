package ru.edu.task4.xml;

public class CacheService implements SomeInterface {

    private SomeInterface delegate;

    public CacheService(SomeInterface delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getName() {
        return "cacheService of " + delegate.getName();
    }
}
