package ru.edu.task5.common;

/**
 * ReadOnly
 */
public class ImplementTwo implements InterfaceToWrap {
    @Override
    public String getValue() {
        return "two";
    }
}
