package ru.edu.task3.xml;

/**
 * ReadOnly
 */
public class MainContainer {

    private DependencyObject dependency;

    public MainContainer(DependencyObject dependency) {
        this.dependency = dependency;
    }

    public String getValue() {
        return dependency.getValue();
    }
}
