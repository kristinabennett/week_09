package ru.edu.task1.xml;

/**
 * ReadOnly
 */
public class ComponentB {

    private String string;

    public ComponentB(String string) {
        this.string = string;
    }

    public boolean isValid() {
        return "stringForComponentB".equals(string);
    }
}
