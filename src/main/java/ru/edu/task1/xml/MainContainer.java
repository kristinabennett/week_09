package ru.edu.task1.xml;

/**
 * ReadOnly
 */
public class MainContainer {

    private ComponentA componentA;

    private ComponentB componentB;

    public MainContainer(ComponentA a, ComponentB b) {
        componentA = a;
        componentB = b;
    }

    public boolean isValid() {
        return componentA != null && componentB != null && componentA.isValid() && componentB.isValid();
    }
}
